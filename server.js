var express = require('express');
require('dotenv').config();
var connection = require('./db/database');
var session = require('express-session');
var bodyParser = require('body-parser');



var app = express();

var app = express();
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.set('view engine','ejs');
app.use(express.static('public'))


app.post('/auth', function(request, response) {
	var username = request.body.username;
	var password = request.body.password;
	if (username && password) {
		connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				response.redirect('/home');
			} else {
				response.send('Incorrect Username and/or Password!');
			}			
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});
app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.render('pages/index',{title: "Reconhecimento Facial"});
	} else {
		response.send('Please login to view this page!');
	}
	response.end();
});
app.get('/',function(req,res){
    res.render('pages/login',{title: "Login"});
})



app.listen(3000,()=> console.log('App is Running...'));